import matplotlib.pyplot as plt
import numpy as np

import csv, sys
filename = 'mesures.csv'
rows = []
with open(filename, newline='') as f:
    reader = csv.reader(f)
    try:
        for row in reader:
            rows.append(row)
    except csv.Error as e:
        sys.exit('file {}, line {}: {}'.format(filename, reader.line_num, e))

bufferFramesDict = set({})
filterSizeDict = set({})
for row in rows[1:] :
    bufferFramesDict.add(int(row[0]))
    filterSizeDict.add(int(row[2]))
bufferFramesVect = sorted(list(bufferFramesDict))
filterSizeVect = sorted(list(filterSizeDict))

print(rows[0])

plt.figure()
plt.title("temps processus en fonction de la taille de la réponse")

for bufferFrames in bufferFramesVect :
    ptimes = []
    for row in rows[1:]:
        if int(row[0]) == bufferFrames :
            ptimes.append(float(row[3]))
    plt.plot(filterSizeVect, ptimes, marker="x")
    plt.text(bufferFramesVect[0], ptimes[0]*0.9, f"L = {bufferFrames}")
    
plt.xlabel("taille de la réponse")
plt.ylabel("temps processus (s)")
plt.xscale("log")
plt.yscale("log")
plt.grid()
    

plt.figure()
plt.title("temps processus en fct de la taille du buffer")

for filterSize in filterSizeVect :
    ptimes = []
    underruns = []
    for row in rows[1:]:
        if int(row[2]) == filterSize :
            ptimes.append(float(row[3]))
    plt.plot(bufferFramesVect, ptimes, marker="x")
    ##
    plt.text(bufferFramesVect[0], ptimes[0]*0.9, f"M = {filterSize}")
plt.xlabel("taille du buffer")
plt.ylabel("temps processus")
plt.plot(bufferFramesVect, np.array(bufferFramesVect)/44100, c="red")
plt.fill_between(bufferFramesVect, np.array(bufferFramesVect)/44100, plt.gca().get_ylim()[1], color="red", alpha=0.3)
plt.xscale("log")
plt.yscale("log")
plt.grid()

plt.show()
    
