#include "rtaudio-5.2.0/RtAudio.h"
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include "somefunc.h"
#include "processing.h"
#include <vector>
#include <unistd.h>
#include <algorithm>

#define USE_FFT 1

/*
typedef char MY_TYPE;
#define FORMAT RTAUDIO_SINT8
*/

/*
typedef signed short MY_TYPE;
#define FORMAT RTAUDIO_SINT16
*/

/*
typedef S24 MY_TYPE;
#define FORMAT RTAUDIO_SINT24

typedef signed long MY_TYPE;
#define FORMAT RTAUDIO_SINT32

typedef float MY_TYPE;
#define FORMAT RTAUDIO_FLOAT32
*/

typedef double MY_TYPE;
#define FORMAT RTAUDIO_FLOAT64


class CallbackData {
  public:
  unsigned int fs;
  FxChain* fxChain;
  std::vector<double> ptime;
};

void usage( void ) {
  // Error function in case of incorrect command-line
  // argument specifications
  std::cout << "\nuseage: duplex N fs <iDevice> <oDevice> <iChannelOffset> <oChannelOffset>\n";
  std::cout << "    where N = number of channels,\n";
  std::cout << "    fs = the sample rate,\n";
  std::cout << "    iDevice = optional input device to use (default = 0),\n";
  std::cout << "    oDevice = optional output device to use (default = 0),\n";
  std::cout << "    iChannelOffset = an optional input channel offset (default = 0),\n";
  std::cout << "    and oChannelOffset = optional output channel offset (default = 0).\n\n";
  exit( 0 );
}


int inout( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
           double /*streamTime*/, RtAudioStreamStatus status, void *data )
{

  if ( status ) std::cout << "Stream over/underflow detected." << std::endl;
  double tic = get_process_time();
  double toc;

  CallbackData* data_p = (CallbackData*) data;

  //for (int i = 0 ; i < nBufferFrames ; i++) std::cout << ((double*)inputBuffer)[i] << std::endl;
  
  data_p->fxChain->setInput((double*)inputBuffer);
  data_p->fxChain->processBuffer();
  data_p->fxChain->copyOutput((double*)outputBuffer);
  
  toc = get_process_time();
  // std::cout << "Time elapsed: " << toc-tic << "\tBlock duration: " << (double)nBufferFrames / data_p->fs << std::endl;
  // if (toc - tic > (double)nBufferFrames / data_p->fs) {
  //   std::cout << "Callback underrun detected!" << std::endl;
  // }
  data_p->ptime.push_back(toc - tic);

  return 0;
}
 

int main() {

  //std::vector<unsigned int> bufFramesVec{128, 256, 512, 1024, 1536, 2048, 3184, 4096, 6144, 8192};
  std::vector<unsigned int> bufFramesVec{128,256,512,1024,2048,4096,8192,16384,32768,65536};
  std::vector<unsigned int> filterSizeVec{128,256,512,1024,2048,4096,8192,16384,32768,65536};

  unsigned int fs, oDevice = 0, iDevice = 0, iOffset = 0, oOffset = 0;
  
  RtAudio adac;
  if ( adac.getDeviceCount() < 1 ) {
    std::cout << "\nNo audio devices found!\n";
    exit( 1 );
  }

  fs = 44100;
  RtAudio::StreamParameters iParams, oParams;
  iParams.deviceId = iDevice;
  iParams.nChannels = 1;
  iParams.firstChannel = iOffset;
  oParams.deviceId = oDevice;
  oParams.nChannels = 1;
  oParams.firstChannel = oOffset;
  if ( iDevice == 0 )
    iParams.deviceId = adac.getDefaultInputDevice();
  if ( oDevice == 0 )
    oParams.deviceId = adac.getDefaultOutputDevice();

  RtAudio::StreamOptions options;
  //options.flags |= RTAUDIO_NONINTERLEAVED;

  // Let RtAudio print messages to stderr.
  adac.showWarnings( true );

  std::ofstream csv_file;
  csv_file.open("mesures.csv");
  csv_file << "bufferFrames,blockDuration,filterSize,processTime,processTimeMedian" << std::endl;

  for (auto bufFrames : bufFramesVec) {
    for (auto filterSize : filterSizeVec) {
      CallbackData data;
      data.fs = fs;
      double filter[filterSize];
      for (unsigned int i = 0 ; i < filterSize ; i++) {
        filter[i] = rand() / (double)RAND_MAX;
      }
      FxChain fxChain(bufFrames);
      data.fxChain = &fxChain;
      ConvolveEffect reverb(bufFrames, NULL, filterSize, filter);
      fxChain.push_back(&reverb);
      try {
        adac.openStream( &oParams, &iParams, FORMAT, fs, &bufFrames, &inout, (void *)&data, &options );
      }
      catch ( RtAudioError& e ) {
        std::cout << '\n' << e.getMessage() << '\n' << std::endl;
        exit( 1 );
      }

      try {
        adac.startStream();

        std::cout << "\nRunning ... (buffer frames = " << bufFrames << ", filter size = " << filterSize <<  ").\n";
        sleep(10);
        double mean = 0;
        std::vector<double> copy(data.ptime);
        std::sort(copy.begin(), copy.end());
        double median = copy[copy.size()/2+1];
        for (auto t : data.ptime) { mean += t; }
        mean /= data.ptime.size();
        double btime = bufFrames / (double)fs;
        csv_file << bufFrames << "," << btime << "," << filterSize << "," << mean << "," << median << std::endl;


        // Stop the stream.
        adac.stopStream();
      }
      catch ( RtAudioError& e ) {
        std::cout << '\n' << e.getMessage() << '\n' << std::endl;
        goto cleanup;
      }

    cleanup:
      if ( adac.isStreamOpen() ) adac.closeStream();
    }
  }

  csv_file.close();

  return 0;
}
